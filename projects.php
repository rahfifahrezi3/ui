<?php
    include "koneksi.php";
    session_start();
    if(isset($_SESSION['nama_petugas'])){
        
?>
<html lang="en" dir="ltr"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>UI</title>


    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- App CSS -->
    <link type="text/css" href="assets/css/app.css" rel="stylesheet">
    <link type="text/css" href="assets/css/app.rtl.css" rel="stylesheet">

    <!-- Simplebar -->
    <link type="text/css" href="assets/vendor/simplebar.css" rel="stylesheet">
</head>

<body>
    <div class="fixed-top header-projects">

        <nav class="navbar navbar-expand-md navbar-light bg-white d-flex-none">
            <div class="media align-items-center">
                <a href="index.html" class="drawer-brand-circle mr-2">S</a>
                <div class="media-body">
                    <a href="index.html" class="page-title m-0">UInvent - Peminjaman</a>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="mainNavbar">
                <ul class="navbar-nav ml-auto  align-items-center">
                    <li class="nav-item nav-link">
                        <a class="btn btn-outline-primary" href="index.php">
          <i class="material-icons align-middle md-18">chevron_left</i>
          Back to Dashboard
        </a>
                    </li>

                    <li class="nav-item nav-divider">
                        </li><li class="nav-item dropdown nav-dropdown d-flex align-items-center">
                            <a href="#" class="nav-link dropdown-toggle dropdown-clear-caret" data-toggle="dropdown" aria-expanded="false">
          Account
          <img src="../../../pbs.twimg.com/profile_images/928893978266697728/3enwe0fO_400x400.jpg" class="img-fluid rounded-circle ml-1" width="35" alt="">
        </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-account">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="profile.html" class="dropdown-item ">
                <i class="material-icons md-18 align-middle mr-1">account_circle</i>
                <span class="align-middle">Profile</span>
              </a>
                                    </li>
                                    <li>
                                        <a href="login.html" class="dropdown-item">
                <i class="material-icons md-18 align-middle mr-1">exit_to_app</i>
                <span class="align-middle">Logout</span>
              </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                </ul>
            </div>
        </nav>
    </div>

    <!-- main content -->
    <div class="top-navbar">

        <nav class="navbar navbar-expand-md bg-primary navbar-dark d-flex  flex-column aling-items-center mb-3">
            <button class="btn btn-link text-white pl-0 d-md-none" type="button" data-toggle="sidebar">
    <i class="material-icons align-middle md-36">short_text</i>
  </button>
    <p>
            <h1 class="h2 mb-0">Peminjaman</h1>
        </p>
        </nav>

        <div class="container mt-0 pb-3">

            <hr>
            <div class="card card-body p-2">
                <div class="row row-projects">
                    <div class="col">
                        <i class="material-icons text-link-color md-36">dvr</i>
                        <div class="mb-1">Laptop</div>
                        <h4 class="mb-0 ">70</h4>
                    </div>
                    <div class="col">
                        <i class="material-icons text-success md-36">cast</i>
                        <div class="mb-1">Proyektor</div>
                        <h4 class="mb-0">4</h4>
                    </div>
                    <div class="col">
                        <i class="material-icons text-warning md-36">assistant_photo</i>
                        <div class="mb-1">Buku</div>
                        <h4 class="mb-0">1000</h4>
                    </div>
                    <div class="col">
                        <i class="material-icons text-primary md-36">contacts</i>
                        <div class="mb-1">Alat</div>
                        <h4 class="mb-0">20</h4>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-6">
                    <small class="text-muted">Showing 1 to 6 of 6 records</small>
                </div>
                <div class="col-md-6 text-md-right">
                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                        <div class="dropdown mr-1">
                            <button id="sortDropdown" type="button" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sort By
          </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sortDropdown">
                                <a class="dropdown-item" href="#">Date</a>
                                <a class="dropdown-item" href="#">Name</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button id="filterDropdown" type="button" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Filter by
          </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="filterDropdown">
                                <a class="dropdown-item" href="#">Active</a>
                                <a class="dropdown-item" href="#">Inactive</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-projects mb-0">
                        <thead>
                            <tr>
                                <th>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input check-projects" id="project-check">
                                        <label class="custom-control-label" for="project-check"></label>
                                    </div>
                                </th>
                                <th>Nama Barang</th>
                                <th>Kode Inventaris</th>
                                <th>Ruang</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            <!-- <tr>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input project" id="project-check1">
                                        <label class="custom-control-label" for="project-check1"></label>
                                    </div>
                                </td>
                            </tr> -->

                                        <?php
                                            include "koneksi.php";
                                            
                                            $no=1;
                                            $data=mysqli_query($koneksi,"SELECT * FROM inventaris i join ruang r ON i.id_ruang=r.id_ruang
                                                JOIN jenis j ON i.id_jenis=j.id_jenis JOIN petugas p ON i.id_petugas=p.id_petugas");
                                            while ($tampil=mysqli_fetch_array($data)) {
                                                echo "<tr>";
                                                echo "<td>$no</td>";
                                                echo "<td>$tampil[nama]</td>";
                                                echo "<td>$tampil[kode_inventaris]</td>";
                                                echo "<td>$tampil[nama_ruang]</td>";

                                               echo "<td>
                                                <a href='pinjam.php?id=$tampil[id_inventaris]'>
                                                <button class='btn btn-primary'>Pinjam</button>
                                                </a>
                                                </td>";
                                                echo "</tr>";
                                            $no++;
                                            }
                                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- drawer -->
    <div class="mdk-drawer js-mdk-drawer" id="default-drawer" data-align="start" data-position="left" data-domfactory-upgraded="mdk-drawer"><div class="mdk-drawer__scrim" style=""></div>
        <div class="mdk-drawer__content" style="">
            <div class="mdk-drawer__inner" data-simplebar="" data-simplebar-force-enabled="true">

                <nav class="drawer  drawer--dark">
                    <div class="drawer-spacer">
                        <div class="media align-items-center">
                            <a href="index.html" class="drawer-brand-circle mr-2">S</a>
                            <div class="media-body">
                                <a href="index.html" class="h5 m-0 text-link">Sub Pro - Admin</a>
                            </div>
                        </div>
                    </div>
                    <!-- HEADING -->
                    <div class="py-2 drawer-heading">
                        Dashboards
                    </div>
                    <!-- MENU -->
                    <ul class="drawer-menu" id="dasboardMenu" data-children=".drawer-submenu">
                        <li class="drawer-menu-item ">
                            <a href="index.html">
        <i class="material-icons">poll</i>
        <span class="drawer-menu-text"> Financial</span>
      </a>
                        </li>
                        <li class="drawer-menu-item">
                            <a href="projects.html">
        <i class="material-icons">dns</i>
        <span class="drawer-menu-text"> Projects/Tickets</span>
        <span class="badge badge-pill badge-success ml-1">4</span>
      </a>
                        </li>
                        <li class="drawer-menu-item ">
                            <a href="retail.html">
        <i class="material-icons">store</i>
        <span class="drawer-menu-text"> E-Commerce</span>
      </a>
                        </li>
                        <li class="drawer-menu-item ">
                            <a href="real-estate-grid.html">
        <i class="material-icons">business</i>
        <span class="drawer-menu-text"> Real Estate</span>
      </a>
                        </li>
                        <li class="drawer-menu-item ">
                            <a href="profile.html">
        <i class="material-icons">pages</i>
        <span class="drawer-menu-text"> Social</span>
      </a>
                        </li>
                    </ul>

                    <!-- HEADING -->
                    <div class="py-2 drawer-heading">
                        Components
                    </div>

                    <!-- MENU -->
                    <ul class="drawer-menu" id="mainMenu" data-children=".drawer-submenu">
                        <li class="drawer-menu-item drawer-submenu">
                            <a data-toggle="collapse" data-parent="#mainMenu" href="#" data-target="#uiComponentsMenu" aria-controls="uiComponentsMenu" aria-expanded="false" class="collapsed">
        <i class="material-icons">library_books</i>
        <span class="drawer-menu-text"> UI Components</span>
      </a>
                            <ul class="collapse " id="uiComponentsMenu">
                                <li class="drawer-menu-item "><a href="ui-buttons.html">Buttons</a></li>
                                <li class="drawer-menu-item "><a href="ui-colors.html">Colors</a></li>
                                <li class="drawer-menu-item "><a href="ui-grid.html">Grid</a></li>
                                <li class="drawer-menu-item "><a href="ui-icons.html">Icons</a></li>
                                <li class="drawer-menu-item "><a href="ui-typography.html">Typography</a></li>
                                <li class="drawer-menu-item "><a href="ui-drag-drop.html">Drag &amp; Drop</a></li>
                                <li class="drawer-menu-item "><a href="ui-loaders.html">Loaders</a></li>
                            </ul>
                        </li>


                        <li class="drawer-menu-item drawer-submenu">
                            <a data-toggle="collapse" data-parent="#mainMenu" href="#" data-target="#formsMenu" aria-controls="formsMenu" aria-expanded="false" class="collapsed">
        <i class="material-icons">text_format</i>
        <span class="drawer-menu-text"> Forms</span>
      </a>
                            <ul class="collapse " id="formsMenu">
                                <li class="drawer-menu-item "><a href="form-controls.html">Form Controls</a></li>
                                <li class="drawer-menu-item "><a href="checkboxes-radios.html">Checkboxes &amp; Radios</a></li>
                                <li class="drawer-menu-item "><a href="switches-toggles.html">Switches &amp; Toggles</a></li>
                                <li class="drawer-menu-item "><a href="form-layout.html">Layout Variations</a></li>
                                <li class="drawer-menu-item "><a href="validation.html">Validation</a></li>
                                <li class="drawer-menu-item "><a href="custom-forms.html">Custom Forms</a></li>
                                <li class="drawer-menu-item "><a href="text-editor.html">Text Editor</a></li>
                                <li class="drawer-menu-item "><a href="datepicker.html">Datepicker</a></li>
                            </ul>
                        </li>
                        <li class="drawer-menu-item  ">
                            <a href="ui-tables.html">
        <i class="material-icons">tab</i>
        <span class="drawer-menu-text"> Tables</span>
      </a>
                        </li>
                        <li class="drawer-menu-item  ">
                            <a href="ui-notifications.html">
        <i class="material-icons">notifications</i>
        <span class="drawer-menu-text"> Notifications</span>
      </a>
                        </li>
                        <li class="drawer-menu-item  ">
                            <a href="charts.html">
        <i class="material-icons">equalizer</i>
        <span class="drawer-menu-text"> Charts</span>
      </a>
                        </li>
                        <li class="drawer-menu-item  ">
                            <a href="events-calendar.html">
        <i class="material-icons">event_available</i>
        <span class="drawer-menu-text"> Calendar</span>
      </a>
                        </li>
                        <li class="drawer-menu-item  ">
                            <a href="maps.html">
        <i class="material-icons">pin_drop</i>
        <span class="drawer-menu-text"> Maps</span>
      </a>
                        </li>
                    </ul>


                    <!-- HEADING -->
                    <div class="py-2 drawer-heading">
                        Pages
                    </div>

                    <!-- MENU -->
                    <ul class="drawer-menu" id="mainMenu" data-children=".drawer-submenu">
                        <li class="drawer-menu-item">
                            <a href="account.html">
        <i class="material-icons">edit</i>
        <span class="drawer-menu-text">Edit Account</span>
      </a>
                        </li>
                        <li class="drawer-menu-item">
                            <a href="login.html">
        <i class="material-icons">lock</i>
        <span class="drawer-menu-text">Login</span>
      </a>
                        </li>
                        <li class="drawer-menu-item">
                            <a href="signup.html">
        <i class="material-icons">account_circle</i>
        <span class="drawer-menu-text">Sign Up</span>
      </a>
                        </li>
                        <li class="drawer-menu-item">
                            <a href="forgot-password.html">
        <i class="material-icons">help</i>
        <span class="drawer-menu-text">Forgot Password</span>
      </a>
                        </li>
                    </ul>

                </nav>
            </div>
        </div>
    </div>
    <!-- // END drawer -->

    <!-- jQuery -->
    <script src="assets/vendor/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/vendor/popper.js"></script>
    <script src="assets/vendor/bootstrap.min.js"></script>

    <!-- APP -->
    <script src="assets/js/color_variables.js"></script>
    <script src="assets/js/app.js"></script>


    <script src="assets/vendor/dom-factory.js"></script>
    <!-- DOM Factory -->
    <script src="assets/vendor/material-design-kit.js"></script>
    <!-- MDK -->

    <script>
        (function() {
            'use strict';
            // Self Initialize DOM Factory Components
            domFactory.handler.autoInit()


            // Connect button(s) to drawer(s)
            var sidebarToggle = document.querySelectorAll('[data-toggle="sidebar"]')

            sidebarToggle.forEach(function(toggle) {
                toggle.addEventListener('click', function(e) {
                    var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer'
                    var drawer = document.querySelector(selector)
                    if (drawer) {
                        if (selector == '#default-drawer') {
                            $('.container-fluid').toggleClass('container--max');
                        }
                        drawer.mdkDrawer.toggle();
                    }
                })
            })
        })()
    </script>


    <script>
        $(".check-projects").click(function() {
            $('.project').not(this).prop('checked', this.checked);
        });
    </script>




</body>
</html>
<?php
}?>