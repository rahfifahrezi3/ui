<?php
    include "koneksi.php";
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>UI</title>


    <link type="text/css" href="assets/css/vendor-morris.css" rel="stylesheet">
    <link type="text/css" href="assets/css/vendor-bootstrap-datepicker.css" rel="stylesheet">

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- App CSS -->
    <link type="text/css" href="assets/css/app.css" rel="stylesheet">
    <link type="text/css" href="assets/css/app.rtl.css" rel="stylesheet">

    <!-- Simplebar -->
    <link type="text/css" href="assets/vendor/simplebar.css" rel="stylesheet">

</head>

<body>
    <div class="mdk-drawer-layout js-mdk-drawer-layout" data-fullbleed data-push data-responsive-width="992px" data-has-scrolling-region>

        <div class="mdk-drawer-layout__content">
            <!-- header-layout -->
            <div class="mdk-header-layout js-mdk-header-layout  mdk-header--fixed  mdk-header-layout__content--scrollable">
                <!-- header -->
                <div class="mdk-header js-mdk-header bg-primary" data-fixed>
                    <div class="mdk-header__content">

                        <nav class="navbar navbar-expand-md bg-primary navbar-dark d-flex-none">
                            <button class="btn btn-link text-white pl-0" type="button" data-toggle="sidebar">
    <i class="material-icons align-middle md-36">short_text</i>
  </button>
                            <div class="page-title m-0">Inventarisir</div>

                            <div class="collapse navbar-collapse" id="mainNavbar">
                                <ul class="navbar-nav ml-auto align-items-center">
                                    <li class="nav-item nav-link">
                                    <li class="nav-item nav-divider">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link dropdown-toggle dropdown-clear-caret" data-toggle="sidebar" data-target="#user-drawer">
                                        
          Account
          <img src="../../../pbs.twimg.com/profile_images/928893978266697728/3enwe0fO_400x400.jpg" class="img-fluid rounded-circle ml-1" width="35"
            alt="">
        </a>
                                        </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>

                <!-- content -->
                <div class="mdk-header-layout__content top-navbar mdk-header-layout__content--scrollable h-100">
                    <!-- main content -->




                    <div class="container-fluid">
                        <div class="row font-1">
                            <div class="col-lg-3">
                                <div class="card card-body flex-row align-items-center">
                                    <h5 class="m-0"><i class="material-icons align-middle text-muted md-18">laptop</i> Laptop</h5>
                                    <div class="text-primary ml-auto">75</div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="card card-body flex-row align-items-center">
                                    <h5 class="m-0"><i class="material-icons align-middle text-muted md-18">dns</i> Proyektor</h5>
                                    <div class="text-primary ml-auto">5</div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="card card-body flex-row align-items-center">
                                    <h5 class="m-0"><i class="material-icons align-middle text-muted md-18">book</i> Buku</h5>
                                    <div class="text-primary ml-auto">100</div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="card card-body flex-row align-items-center">
                                    <h5 class="m-0"><i class="material-icons align-middle text-muted md-18">view_quilt</i> Alat 
                                    </h5>
                                    <div class="text-primary ml-auto">200</div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-earnings">
                            <div class="card-group">
                            </div>
                        </div>
                            <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    Table head options
                                </h4>
                            </div>
                            <div class="card-body">

                            <?php
                            	include "koneksi.php";
                            	$id = $_GET['id'];
                            	$data = mysqli_query($koneksi,"SELECT * FROM inventaris WHERE id_inventaris='$id'");
                            	while ($d=mysqli_fetch_array($data)){
                            		?>
                            		<form method="post" action="proses_edit.php">
                            			<table>
                            				<tr>
                            					<td>Nama</td>
                            					<td>
                            						<input type="hidden" name="id" value="<?php echo $d['id_inventaris']; ?>">
                            						<input type="text" name="nama" class="form-control" value="<?php echo $d['nama']; ?>">
                            					</td>
                            				</tr>
                            				<tr>
                            					<td>Kondisi</td>
                            					<td>
                            						<input type="text" name="kondisi" class="form-control" value="<?php echo $d['kondisi']; ?>">
                            					</td>
                            				</tr>
                            				<tr>
                            					<td>Keterangan</td>
                            					<td>
                            						<input type="text" name="keterangan" class="form-control" value="<?php echo $d['keterangan']; ?>">
                            					</td>
                            				</tr>
                            				<tr>
                            					<td>Jumlah</td>
                            					<td>
                            						<input type="text" name="jumlah" class="form-control" value="<?php echo $d['jumlah']; ?>">
                            					</td>
                            				</tr>

                            				<tr>
                            					<td>Tanggal Register</td>
                            					<td>
                            						<input type="text" name="tanggal_register" class="form-control" value="<?php echo $d['tanggal_register']; ?>">
                            					</td>
                            				</tr>
                            				<tr>
                            					<td>Ruang</td>
                            					<td>
                            						<input type="text" name="id_ruang" class="form-control" value="<?php echo $d['id_ruang']; ?>">
                            					</td>
                            				</tr>
                            				<tr>
                            					<td>Kode Inventaris</td>
                            					<td>
                            						<input type="text" name="kode_inventaris" class="form-control" value="<?php echo $d['kode_inventaris']; ?>">
                            					</td>
                            				</tr>
                            				<tr>
                            					<td>Petugas</td>
                            					<td>
                            						<input type="text" name="id_petugas" class="form-control" value="<?php echo $d['id_petugas']; ?>">
                            					</td>
                            				</tr>

                            				<tr>
                            					<td></td>
                            					<td>
                            						<input type="submit" class="btn btn-primary" value="Simpan">
                            					</td>
                            				</tr>
                            			</table>
                            		</form> 
                            	<?php
                            	}
                            	?>
                            

                            </div>
                            </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- // END drawer-layout__content -->

        <!-- drawer -->
        <div class="mdk-drawer js-mdk-drawer" id="default-drawer">
            <div class="mdk-drawer__content">
                <div class="mdk-drawer__inner" data-simplebar data-simplebar-force-enabled="true">

                    <nav class="drawer  drawer--dark">
                        <div class="drawer-spacer">
                            <div class="media align-items-center">
                                <a href="index.html" class="drawer-brand-circle mr-2">UI</a>
                                <div class="media-body">
                                    <a href="index.html" class="h5 m-0 text-link">UInvent</a>
                                </div>
                            </div>
                        </div>
                        <!-- HEADING -->
                        <div class="py-2 drawer-heading">
                            Dashboards
                        </div>
                        <!-- MENU -->
                        <ul class="drawer-menu" id="dasboardMenu" data-children=".drawer-submenu">
                            <li class="drawer-menu-item active ">
                                <a href="index.php">
        <i class="material-icons">poll</i>
        <span class="drawer-menu-text"> Inventarisir</span>
      </a>
                            </li>
                            <li class="drawer-menu-item">
                                <a href="projects.html">
        <i class="material-icons">payment</i>
        <span class="drawer-menu-text"> Peminjaman</span>
        <span class="badge badge-pill badge-success ml-1">4</span>
      </a>
                            </li>
                            <li class="drawer-menu-item ">
                                <a href="retail.html">
        <i class="material-icons">clear_all</i>
        <span class="drawer-menu-text"> Pengembalian</span>
      </a>
                            </li>
                            <li class="drawer-menu-item ">
                                <a href="real-estate-grid.html">
        <i class="material-icons">business</i>
        <span class="drawer-menu-text"> Laporan</span>
      </a>
                            </li>

                        </ul>


                        <!-- HEADING -->
                    </nav>
                </div>
            </div>
        </div>
        <!-- // END drawer -->

        <!-- drawer -->
        <div class="mdk-drawer js-mdk-drawer" id="user-drawer" data-position="right" data-align="end">
            <div class="mdk-drawer__content">
                <div class="mdk-drawer__inner" data-simplebar data-simplebar-force-enabled="true">
                    <nav class="drawer drawer--light">
                        <div class="drawer-spacer drawer-spacer-border">
                            <div class="media align-items-center">
                                <img src="../../../pbs.twimg.com/profile_images/928893978266697728/3enwe0fO_400x400.jpg" class="img-fluid rounded-circle mr-2" width="35" alt="">
                                <div class="media-body">
                                    <a href="#" class="h5 m-0">Account</a>
                                    <div>Account Manager</div>
                                </div>
                            </div>
                        </div>
                        <!-- MENU -->
                        <ul class="drawer-menu" id="userMenu" data-children=".drawer-submenu">

                            <li class="drawer-menu-item">
                                <a href="profile.html">
        <i class="material-icons">account_circle</i>
        <span class="drawer-menu-text"> Profile</span>
      </a>
                            </li>
                            <li class="drawer-menu-item">
                                <a href="login.html">
        <i class="material-icons">exit_to_app</i>
        <span class="drawer-menu-text"> Logout</span>
      </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- // END drawer -->

    </div>
    <!-- // END drawer-layout -->



    <!-- jQuery -->
    <script src="assets/vendor/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/vendor/popper.js"></script>
    <script src="assets/vendor/bootstrap.min.js"></script>

    <!-- Simplebar -->
    <!-- Used for adding a custom scrollbar to the drawer -->
    <script src="assets/vendor/simplebar.js"></script>


    <!-- Vendor -->
    <script src="assets/vendor/Chart.min.js"></script>
    <script src="assets/vendor/moment.min.js"></script>

    <!-- APP -->
    <script src="assets/js/color_variables.js"></script>
    <script src="assets/js/app.js"></script>


    <script src="assets/vendor/dom-factory.js"></script>
    <!-- DOM Factory -->
    <script src="assets/vendor/material-design-kit.js"></script>
    <!-- MDK -->



    <script>
        (function() {
            'use strict';
            // Self Initialize DOM Factory Components
            domFactory.handler.autoInit()


            // Connect button(s) to drawer(s)
            var sidebarToggle = document.querySelectorAll('[data-toggle="sidebar"]')

            sidebarToggle.forEach(function(toggle) {
                toggle.addEventListener('click', function(e) {
                    var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer'
                    var drawer = document.querySelector(selector)
                    if (drawer) {
                        if (selector == '#default-drawer') {
                            $('.container-fluid').toggleClass('container--max');
                        }
                        drawer.mdkDrawer.toggle();
                    }
                })
            })
        })()
    </script>


    <script src="assets/vendor/morris.min.js"></script>
    <script src="assets/vendor/raphael.min.js"></script>
    <script src="assets/vendor/bootstrap-datepicker.min.js"></script>
    <script src="assets/js/datepicker.js"></script>

    <script>
        $(function() {
            window.morrisDashboardChart = new Morris.Area({
                element: 'morris-area-chart',
                data: [{
                        year: '2017-01',
                        a: 6352.27
                    },
                    {
                        year: '2017-02',
                        a: 4309.98
                    },
                    {
                        year: '2017-03',
                        a: 1465.98
                    },
                    {
                        year: '2017-04',
                        a: 1298.25
                    },
                    {
                        year: '2017-05',
                        a: 3209
                    },
                    {
                        year: '2017-06',
                        a: 2083
                    },
                    {
                        year: '2017-07',
                        a: 1285.23
                    },
                    {
                        year: '2017-08',
                        a: 1289
                    },
                    {
                        year: '2017-09',
                        a: 4430
                    },
                    {
                        year: '2017-10',
                        a: 8921.19
                    }
                ],
                xkey: 'year',
                ykeys: ['a'],
                labels: ['Earnings'],
                lineColors: ['#fff'],
                fillOpacity: '0.2',
                gridEnabled: true,
                gridTextColor: '#ffffff',
                resize: true
            });

        });
    </script>

        <script src="assets/vendor/jquery.dataTables.js"></script>
    <script src="assets/vendor/dataTables.bootstrap4.js"></script>

    <script>
        $('#data-table').dataTable();
    </script>

</body>

</html>
